def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t ihebdockerid/java-maven-app:jma-2.3 .'
        sh "echo $password | docker login -u $username --password-stdin "
        sh 'docker push ihebdockerid/java-maven-app:jma-2.1'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this
